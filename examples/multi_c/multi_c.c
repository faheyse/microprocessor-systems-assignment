#include <stdio.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"          // Required for using single-precision variables.
#include "pico/double.h"        // Required for using double-precision variables.
#include "pico/multicore.h"    // Required for using multiple cores on the RP2040
#include "hardware/timer.h"

#define FLAG_VALUE 123


void core1_entry() {
    while (1) {
        // Function pointer is passed to us via the FIFO
        // We have one incoming int32_t as a parameter, and will provide an
        // int32_t return value by simply pushing it back on the FIFO
        // which also indicates the result is ready.
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

int32_t factorial(int32_t n) {
    uint32_t t0 = time_us_32() ; 
    int32_t f = 1;
    for (int i = 2; i <= n; i++) {
        f *= i;
    }
    //return f; 
    return time_us_32() - t0;
}

int32_t fibonacci(int32_t n) {
    uint32_t t0 = time_us_32() ; 

    if (n == 0) {
        return 0;
    }
    if (n == 1) {
        return 1;
    }

    int n1 = 0, n2 = 1, n3 = 0;

    for (int i = 2; i <= n; i++) {
        n3 = n1 + n2;
        n1 = n2;
        n2 = n3;
    }
    //return n3;
    return time_us_32() - t0;
}

#define TEST_NUM 10

int main() {
    stdio_init_all();
    printf("%u\n", time_us_32());
    return 0;


    // This example dispatches arbitrary functions to run on the second core
    // To do this we run a dispatcher on the second core that accepts a function
    // pointer and runs it

    multicore_launch_core1(core1_entry);
    multicore_fifo_drain();
    // Now try a different function
    multicore_fifo_push_blocking((uintptr_t) &fibonacci);
    uint32_t t0 = time_us_32() ; 

    multicore_fifo_push_blocking(TEST_NUM);     // when argument id passed, the function is executed in core1_entry
    uint32_t t1 = time_us_32() ; 

    uint32_t res1 = factorial(TEST_NUM);
    uint32_t t2 = time_us_32() ; 
    uint32_t res2 = multicore_fifo_pop_blocking();
    uint32_t t3 = time_us_32() ; 
    printf("\n\nTime taken for factorial (core0): %u\n", t2-t0) ;
    printf("Time taken for fibonacci (core1): %u\n",t3-t0 - (t2-t1) ) ; 
    printf("Total time taken (parallel): %u\n", t3-t0) ;

    return 0;
}
