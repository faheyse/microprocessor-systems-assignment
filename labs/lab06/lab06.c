
#include <stdio.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"          // Required for using single-precision variables.
#include "pico/double.h"        // Required for using double-precision variables.
#include "pico/multicore.h"    // Required for using multiple cores on the RP2040
#include "hardware/timer.h"


int32_t wallis_single(){
//float wallis_single(){
  float n = 1;
  float pi_2 = 1; 

  while (n < 100000) {
//!     4n^2
//!   ---------
//!   4n^2 - 1
    pi_2 *= (4*n*n)/(4*n*n -1) ; 
    
    if (3.14159265359 == 2*pi_2)
        return 2*pi_2;

    n = n+1 ;
  }

  return pi_2*2; 
}

int32_t wallis_double(){
//double wallis_double(){

  double n = 1;
  double pi_2 = 1; 

  while (n < 100000) {

//!     4n^2
//!   ---------
//!   4n^2 - 1

    pi_2 *= (4*n*n)/(4*n*n -1) ; 
    
    if (3.14159265359 == 2*pi_2)
        return 2*pi_2;    

    n = n+1 ;
  }

  return pi_2*2 ; 
}

/**
 * @brief Edit to original code:
 *        core1_entry does not wait for an argument to be pusghed in from fifo.
 *        This way, the time_us_32 snapshot is taken closer to the actual time of execution.
 */
void core1_entry() {
    while (1) {
        // 
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();

        int32_t result = (*func)();
        multicore_fifo_push_blocking(result);
    }
}

// Main code entry point for core0.
int main() {


    stdio_init_all();
    multicore_launch_core1(core1_entry);
  
/**
 * @param t0
 *    start
 */
    uint32_t t0 = time_us_32() ; 
    multicore_fifo_push_blocking((uintptr_t) &wallis_double);
    uint32_t t1 = time_us_32() ; 
    wallis_single() ; 
    uint32_t t2 = time_us_32() ; 
    printf("time taken for wallis single (core 0):   %u\n", t2-t1) ;

/**
 * @brief
 *    pop_blocking() returns when the calculation is finished.
 *    Note the calculation may have finished on core1 while wallis_single() 
 *    was executing on core0, so it is not possible to get acccurate readings for
 *    both functions running on different core in one program.
 *    As a result I found the time taken to get each of the wallis products
 *    by running this program twice, once to get wallis_single and once to get wallis_double.
 */
    multicore_fifo_pop_blocking();
    uint32_t t3 = time_us_32() ; 
    printf("time taken for wallis double (core 1):   %u\n", t3-t0) ;
    /**
 * @param t3
 *    end
 */

    printf("total runtime (parallel):   %u\n", t3-t0) ;

    return 0;
}

