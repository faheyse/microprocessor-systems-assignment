#include <stdio.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"          // Required for using single-precision variables.
#include "pico/double.h"        // Required for using double-precision variables.
#include "pico/multicore.h"    // Required for using multiple cores on the RP2040
#include "hardware/timer.h"
#include "hardware/structs/xip_ctrl.h"
#include "hardware/address_mapped.h"
#include "hardware/regs/xip.h"

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */
void core1_entry() {
    while (1) {
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        //int32_t p = multicore_fifo_pop_blocking();      doesnt take arguments
        //int32_t result = (*func)(p);
        int32_t result = (*func)();
        multicore_fifo_push_blocking(result);
    }
}


int32_t wallis_single(){
//float wallis_single(){
  float n = 1;
  float pi_2 = 1; 

  while (n < 100000) {
//!     4n^2
//!   ---------
//!   4n^2 - 1
    pi_2 *= (4*n*n)/(4*n*n -1) ; 
    
    if (3.14159265359 == 2*pi_2)
        return 2*pi_2;

    n = n+1 ;
  }

  return pi_2*2; 
}

int32_t wallis_double(){
//double wallis_double(){

  double n = 1;
  double pi_2 = 1; 

  while (n < 100000) {

//!     4n^2
//!   ---------
//!   4n^2 - 1

    pi_2 *= (4*n*n)/(4*n*n -1) ; 
    
    if (3.14159265359 == 2*pi_2)
        return 2*pi_2;    

    n = n+1 ;
  }

  return pi_2*2 ; 
}

// Function to get the enable status of the XIP cache
bool get_xip_cache_en(){
    if ( (xip_ctrl_hw->ctrl & 0x00000001) == 0x00000001)
        return true ; 
    else
        return false ; 
}

// Function to set the enable status of the XIP cache
// CTRL register bt 0 (enable bit) 
bool set_xip_cache_en(bool cache_en){
    if (cache_en == true) 
      xip_ctrl_hw->ctrl = (xip_ctrl_hw->ctrl | 0x00000001) ; 
    else
      xip_ctrl_hw->ctrl = (xip_ctrl_hw->ctrl & 0xFFFFFFFE) ; 
    
    //printf("%d\n", xip_ctrl_hw->ctrl) ; 
    return 1 ; 
}


// Main code entry point for core0.
int main() {


    stdio_init_all();
    multicore_launch_core1(core1_entry);

// single core cache disable
    set_xip_cache_en(1) ;   
    uint32_t t0 = time_us_32() ; 
    wallis_single();
    uint32_t t1 = time_us_32() ;
    wallis_double();
    uint32_t t2 = time_us_32() ; 
    printf("single core w/ cache enabled\n") ;
    printf("\twallis_single runtime: %u\n", t1-t0) ; 
    printf("\twallis_double runtime: %u\n",t2-t1 ) ; 
    printf("\ttotal runtime: %u\n\n", t2-t0) ; 

// single core cache disabled
    set_xip_cache_en(0) ;   
    t0 = time_us_32() ; 
    wallis_single();
    t1 = time_us_32() ;
    wallis_double();
    t2 = time_us_32() ; 
    printf("single core w/ cache disabled\n") ;
    printf("\twallis_single runtime: %u\n", t1-t0) ; 
    printf("\twallis_double runtime: %u\n",t2-t1 ) ; 
    printf("\ttotal runtime: %u\n\n", t2-t0) ; 


// both cores cache enabled
    set_xip_cache_en(1) ;   
    t0 = time_us_32() ; 
    multicore_fifo_push_blocking((uintptr_t) &wallis_double);
    wallis_single();
    t1 = time_us_32() ;
    multicore_fifo_pop_blocking() ; 
    t2 = time_us_32() ; 
    printf("both cores w/ cache enabled\n") ;
    printf("\twallis_single runtime: %u\n", t1-t0) ; 
    printf("\twallis_double runtime: %u\n",t2-t0 ) ;    // assume this is accurate if wallis_double take slonger to calculate than wallis_single
    printf("\ttotal runtime: %u\n\n", t2-t0) ; 


// both cores cache disabled
    set_xip_cache_en(0) ;   
    t0 = time_us_32() ; 
    multicore_fifo_push_blocking((uintptr_t) &wallis_double);
    wallis_single();
    t1 = time_us_32() ;
    multicore_fifo_pop_blocking() ; 
    t2 = time_us_32() ; 
    printf("both cores w/ cache disabled\n") ;
    printf("\twallis_single runtime: %u\n", t1-t0) ; 
    printf("\twallis_double runtime: %u\n",t2-t0 ) ;    // assume this is accurate if wallis_double take slonger to calculate than wallis_single
    printf("\ttotal runtime: %u\n\n", t2-t0) ; 

    while (1);



    return 0;
}

