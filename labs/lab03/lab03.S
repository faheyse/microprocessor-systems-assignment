.syntax unified                 @ Specify unified assembly syntax
.cpu    cortex-m0plus           @ Specify CPU type is Cortex M0+
.thumb                          @ Specify thumb assembly for RP2040
.global main_asm                @ Provide program starting address to the linker
.align 4                        @ Specify code alignment

@@@ code to test how stack and pop/push works



main_asm:
    /* Save sp before push. */
    mov     r0, sp

/* Push. */
    ldr     r1, =1
    ldr     r2, =2
    push    {r1, r2}

/* Save sp after push. */
    mov     r1, sp

/* Restore. */
    ldr r3, =0
    ldr r4, =0
    pop {r3, r4}
    cmp r3, #1
    bne fail
    cmp r4, #2
    bne fail

/* Check that stack pointer moved down by 8 bytes
 * (2 registers x 4 bytes each). */
    subs    r0, r1
    cmp     r0, #8
    bne     fail

fail:
    b       fail


@ Set data alignment
.data
    .align 4
